//
//  ViewController.swift
//  TikiMap
//
//  Created by Tuan An on 12/31/15.
//  Copyright © 2015 DZO. All rights reserved.
//

import UIKit
import GoogleMaps
import MLPAutoCompleteTextField
public typealias GetAddressServiceHandler = (String, NSError?) -> Void

class ViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var firstPointAddressTextField: MLPAutoCompleteTextField!
    @IBOutlet weak var secondPointAddressTextField: MLPAutoCompleteTextField!
    @IBOutlet weak var mapView: GMSMapView!
    
    var waypointsArray: Array<String> = []
    let locationManager = CLLocationManager()
    var routePolyline: GMSPolyline!
    var mapTasks = MapTasks()
    var fetcher : GMSPlacesClient!
    var firstMarker : GMSMarker!
    var secondMarker : GMSMarker!
    var curentBounds : GMSCoordinateBounds?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
        mapView.settings.consumesGesturesInView = false
        fetcher = GMSPlacesClient.init()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D, completion: GetAddressServiceHandler) {
        let geocoder = GMSGeocoder()
        var addressStr = ""
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                let lines = address.lines as! [String]
                addressStr = lines.joinWithSeparator(",")
                completion(addressStr,error)
            }
        }
    }

    func clearRoute() {
        if (routePolyline != nil) {
            routePolyline.map = nil
            routePolyline = nil
        }
    }
    
    func recreateRoute() {
        if let _ = routePolyline {
            clearRoute()
            mapTasks.getDirections(mapTasks.originAddress, destination: mapTasks.destinationAddress, waypoints: waypointsArray, travelMode: nil, completionHandler: { (status, success) -> Void in
                if success {
                    self.drawRoute()
                }
                else {
                    let alert = UIAlertController.init(title: nil, message: status, preferredStyle: UIAlertControllerStyle.ActionSheet)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            })
        }
    }
    
    @IBAction func routTapped(sender: AnyObject) {
              
            let origin = self.firstPointAddressTextField.text
            let destination = self.secondPointAddressTextField.text
            
            self.mapTasks.getDirections(origin, destination: destination, waypoints: nil, travelMode: nil, completionHandler: { (status, success) -> Void in
                if success {
                    self.clearRoute()
                    self.drawRoute()
                }
                else {
                    let alert = UIAlertController.init(title: nil, message: status, preferredStyle: UIAlertControllerStyle.ActionSheet)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            })
    }
    
    func drawRoute() {
        let route = mapTasks.overviewPolyline["points"] as! String
        
        let path: GMSPath = GMSPath(fromEncodedPath: route)
        routePolyline = GMSPolyline(path: path)
        routePolyline.map = mapView
    }

}

extension ViewController : MLPAutoCompleteTextFieldDataSource, MLPAutoCompleteTextFieldDelegate {
    func autoCompleteTextField(textField: MLPAutoCompleteTextField!, possibleCompletionsForString string: String!, completionHandler handler: (([AnyObject]!) -> Void)!) {
        fetcher?.autocompleteQuery(textField.text!, bounds: nil, filter: nil, callback: { (results, error: NSError?) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
            }
            if ((results?.count) != nil) {
                handler(results)
            }
        })

    }
    
    func autoCompleteTextField(textField: MLPAutoCompleteTextField!, didSelectAutoCompleteString selectedString: String!, withAutoCompleteObject selectedObject: MLPAutoCompletionObject!, forRowAtIndexPath indexPath: NSIndexPath!) {
            let markerObject = selectedObject as? GMSAutocompletePrediction
            fetcher.lookUpPlaceID((markerObject?.placeID)!, callback: { (placeResult, error) -> Void in
                if error == nil {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        let newMarker = GMSMarker(position: placeResult!.coordinate)
                        newMarker.map = self.mapView
                        newMarker.draggable = true
                        if textField.tag == 1 {
                            if self.firstMarker != nil {
                                self.firstMarker.map = nil
                            }
                            self.firstMarker = newMarker
                            self.firstMarker.title = "A"
                            self.firstMarker.userData = "A"
                        } else {
                            if self.secondMarker != nil {
                                self.secondMarker.map = nil
                            }
                            self.secondMarker = newMarker
                            self.secondMarker.title = "B"
                            self.firstMarker.userData = "B"

                        }
                        self.moveMapCameraToCoordinate(newMarker.position)
                        self.focusForBestView()
                    })
                }
            })
    }
    
    func focusForBestView() {
        if firstMarker != nil && secondMarker != nil {
            let bounds = GMSCoordinateBounds(coordinate: firstMarker.position, coordinate: secondMarker.position)
            self.mapView.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(bounds, withPadding: 40))
        }
    }
    
    func moveMapCameraToCoordinate(coordinate: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.cameraWithTarget(coordinate, zoom: 15)
        self.mapView.camera = camera
    }
}

extension GMSAutocompletePrediction : MLPAutoCompletionObject {
    public func autocompleteString() -> String! {
        return attributedFullText.string
    }
}

extension ViewController: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            locationManager.startUpdatingLocation()
            mapView.myLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
    }
    
    func mapView(mapView: GMSMapView!, didEndDraggingMarker marker: GMSMarker!) {
        reverseGeocodeCoordinate(marker.position) { (newAddressStr, error) -> Void in
            if marker.isEqual(self.firstMarker) {
                self.firstPointAddressTextField.text = newAddressStr
            } else {
                self.secondPointAddressTextField.text = newAddressStr
            }
            print(newAddressStr)
            self.clearRoute()
        }
        
    }
    
    func mapView(mapView: GMSMapView!, didTapOverlay overlay: GMSOverlay!) {
        let circle : GMSCircle = overlay as! GMSCircle
        let touchCoOrdinate : CLLocationCoordinate2D = circle.position
        print(touchCoOrdinate)
    }

}
